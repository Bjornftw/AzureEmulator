﻿using Azure.Manager.Inheritance;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Azure.Manager
{
    sealed class CacheManager : IDisposable
    {
        /// <summary>
        /// Used to grab the items which need to be reloaded.
        /// </summary>
        private List<ICacheable> storage { get; set; }

        /// <summary>
        /// Used for running our task without backing up the main thread. 
        /// </summary>
        private Thread backgroundWorker { get; set; }

        /// <summary>
        /// Is the thread ready to be disposed of?
        /// </summary>
        private Boolean IsReady { get; set; }
        
        /// <summary> 
        /// Safe/readonly access to the list of items which need to be reloaded.
        /// </summary>
        public List<ICacheable> Storage
        {
            get
            {
                return this.storage;
            }
        }

        /// <summary>
        /// Is the backgroundWorker running?
        /// </summary>
        public Boolean IsRunning
        {
            get
            {
                return this.backgroundWorker.IsAlive;
            }
        }
        
        /// <summary>
        /// Default.
        /// </summary>
        public CacheManager()
        {
            this.storage = new List<ICacheable>();
            this.storage.Add(new ClearRoomsCache());
            this.storage.Add(new ClearUserCache());

            this.backgroundWorker = new Thread(this.Run);
            this.backgroundWorker.Priority = ThreadPriority.Lowest;
            this.backgroundWorker.Start();
        }
        
        private void Run()
        {
            while(IsRunning)
            {
                this.IsReady = false;
                var sw = new Stopwatch();
                sw.Start();

                foreach (ICacheable Items in this.storage)
                {
                    Items.Parse();
                }

                sw.Stop();
                Out.WriteLine("Loaded Cache Manager succesfully in " + sw.ElapsedMilliseconds + " ms", "Azure.Cache");
                Console.WriteLine();

                // No idea why I call the Reset() function.
                sw.Reset();

                this.IsReady = true;
                Thread.Sleep(1000000);
            }
        }
        
        /// <summary>
        /// Stop the background thread in an organized fashion.
        /// </summary>
        public void Dispose()
        {
            // Wait for the thread to finish its cycle.
            while(this.IsReady)
            {
                if (!this.IsRunning)
                {
                    break; // So we don't abort the thread twice.
                }

                this.backgroundWorker.Abort();
            }
            
            this.backgroundWorker = null;
        }
    }
}
