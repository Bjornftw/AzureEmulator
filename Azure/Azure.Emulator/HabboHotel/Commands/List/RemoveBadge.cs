﻿#region

using System;
using Azure.Configuration;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class RemoveBadge. This class cannot be inherited.
    /// </summary>
    internal sealed class RemoveBadge : Command
    {
        public RemoveBadge()
        {
            MinParams = 2;
            Description = "Remove user badge.";
            Usage = "[username] [badge]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("manager");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var client = AzureEmulator.GetGame().GetClientManager().GetClientByUserName(pms[0]);
            if (client == null)
            {
                session.SendNotif(TextManager.GetText("user_not_found"));
                return;
            }
            if (!client.GetHabbo().GetBadgeComponent().HasBadge(pms[1]))
            {
                session.SendNotif(TextManager.GetText("command_badge_remove_error"));
                return;
            }
            client.GetHabbo().GetBadgeComponent().RemoveBadge(pms[1], client);
            session.SendNotif(TextManager.GetText("command_badge_remove_done"));
            AzureEmulator.GetGame()
                .GetModerationTool()
                .LogStaffEntry(session.GetHabbo().UserName, client.GetHabbo().UserName,
                    "Badge Taken", string.Format("Badge taken from user [{0}]", pms[1]));
        }
    }
}