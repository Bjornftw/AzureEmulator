﻿#region

using System;
using Azure.Configuration;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class RefreshGroups. This class cannot be inherited.
    /// </summary>
    internal sealed class RefreshGroups : Command
    {
        public RefreshGroups()
        {
            MinParams = 0;
            Description = TextManager.GetText("cmd_update_groups_desc");
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("cmd_global_refresh_permissions");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            AzureEmulator.GetGame().GetGroupManager().InitGroups();
            session.SendWhisper(TextManager.GetText("cmd_succesfully"));
        }
    }
}