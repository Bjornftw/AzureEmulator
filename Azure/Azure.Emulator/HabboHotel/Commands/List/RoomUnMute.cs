﻿#region

using System;
using Azure.HabboHotel.GameClients;
using Azure.Messages;
using Azure.Messages.Parsers;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class RoomUnMute. This class cannot be inherited.
    /// </summary>
    internal sealed class RoomUnMute : Command
    {
        public RoomUnMute()
        {
            MinParams = 0;
            Description = "Unmute room.";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("moderator");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var room = session.GetHabbo().CurrentRoom;
            if (!session.GetHabbo().CurrentRoom.RoomMuted)
            {
                session.SendWhisper("Room isn't muted.");
                return;
            }

            session.GetHabbo().CurrentRoom.RoomMuted = false;
            var message = new ServerMessage();
            message.Init(LibraryParser.OutgoingRequest("AlertNotificationMessageComposer"));
            message.AppendString("Room is now UnMuted.");
            message.AppendString("");
            room.SendMessage(message);
            AzureEmulator.GetGame()
                .GetModerationTool().LogStaffEntry(session.GetHabbo().UserName, string.Empty,
                    "Room Unmute", "Room UnMuted");
        }
    }
}