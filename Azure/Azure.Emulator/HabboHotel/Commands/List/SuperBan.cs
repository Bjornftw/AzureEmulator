﻿#region

using System.Linq;
using Azure.HabboHotel.GameClients;
using Azure.Configuration;
using System;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class SuperBan. This class cannot be inherited.
    /// </summary>
    internal sealed class SuperBan : Command
    {
        public SuperBan()
        {
            MinParams = -1;
            Description = "Ban user by account for ever.";
            Usage = "[username] [reason]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("moderator");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var client = AzureEmulator.GetGame().GetClientManager().GetClientByUserName(pms[0]);
            if (client == null)
            {
                session.SendNotif(TextManager.GetText("user_not_found"));
                return;
            }

            if (client.GetHabbo().Rank >= session.GetHabbo().Rank)
            {
                session.SendNotif(TextManager.GetText("user_is_higher_rank"));
                return;
            }
            AzureEmulator.GetGame()
                .GetModerationTool()
                .LogStaffEntry(session.GetHabbo().UserName, client.GetHabbo().UserName, "Ban",
                    "User has received a Super ban.");
            AzureEmulator.GetGame()
                .GetBanManager()
                .BanUser(client, session.GetHabbo().UserName, 788922000.0, string.Join(" ", pms.Skip(1)),
                    false, false);
        }
    }
}