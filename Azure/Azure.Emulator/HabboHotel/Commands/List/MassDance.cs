﻿#region

using System;
using Azure.Configuration;
using Azure.HabboHotel.GameClients;
using Azure.Messages;
using Azure.Messages.Parsers;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class MassDance. This class cannot be inherited.
    /// </summary>
    internal sealed class MassDance : Command
    {
        public MassDance()
        {
            MinParams = 1;
            Description = "Mass room dance.";
            Usage = "[id]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("manager");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            ushort danceId;
            ushort.TryParse(pms[0], out danceId);

            if (danceId > 4)
            {
                session.SendWhisper(TextManager.GetText("command_dance_wrong_syntax"));
                return;
            }
            var room = AzureEmulator.GetGame().GetRoomManager().GetRoom(session.GetHabbo().CurrentRoomId);
            var roomUsers = room.GetRoomUserManager().GetRoomUsers();

            foreach (var roomUser in roomUsers)
            {
                var message =
                    new ServerMessage(LibraryParser.OutgoingRequest("DanceStatusMessageComposer"));
                message.AppendInteger(roomUser.VirtualId);
                message.AppendInteger(danceId);
                room.SendMessage(message);
                roomUser.DanceId = danceId;
            }
        }
    }
}