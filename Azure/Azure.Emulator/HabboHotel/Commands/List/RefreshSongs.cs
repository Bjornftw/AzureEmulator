﻿#region

using System;
using Azure.Configuration;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.SoundMachine;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class RefreshSongs. This class cannot be inherited.
    /// </summary>
    internal sealed class RefreshSongs : Command
    {
        public RefreshSongs()
        {
            MinParams = 0;
            Description = TextManager.GetText("cmd_update_song_desc");
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("cmd_global_refresh_permissions");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            SongManager.Initialize();
            session.SendWhisper(TextManager.GetText("cmd_succesfully"));
        }
    }
}