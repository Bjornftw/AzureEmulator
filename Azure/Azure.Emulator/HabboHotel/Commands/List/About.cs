﻿#region

using System.Text;
using Azure.HabboHotel.GameClients;
using Azure.Messages;
using Azure.Messages.Parsers;
using Azure.Configuration;
using System.Threading;
using System;
using System.Windows.Forms;


#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class About. This class cannot be inherited.
    /// </summary>
    internal sealed class About : Command
    {
        public About()
        {
            MinParams = 0;
            Description = "Server information";
        }

        public override bool CanExecute(GameClient session)
        {
            return true;
        }

        public override void Execute(GameClient client, string[] pms)
        {
            var userCount = AzureEmulator.GetGame().GetClientManager().Clients.Count;
            var roomsCount = AzureEmulator.GetGame().GetRoomManager().LoadedRooms.Count;
            var roomsname = (roomsCount == 1) ? " Room" : " Rooms";
            client.SendNotifWithPicture("<center> <b><font color=\"#0174DF\" size=\"26\">       Azure</font> <font color=\"#000000\" size=\"18\"> Emulator</font> <font color=\"000000\" size=\"12\"> 2.0 #BETA</font></b></center><br> <font color=\"000000\" size=\"9\" style=\"padding-right:100px;\">                                          Powered by Azure Group</font><br> <b>[Developers] <br />- TimNL         - Jamal        - Diesel        - Boris <br />- Lucca          - Antoine (Tyrex)          - Jaden           - IhToN           - Dominicus           - Cankiee           - Gerard<br />                        [Hotel Statistics]</b><br /> There are currently: " + userCount + " Azure's online in " + roomsCount + roomsname + "<br />", "Azure Emulator", "azure", "", "");
        }
    }
}