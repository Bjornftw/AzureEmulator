﻿#region

using System;
using Azure.Configuration;
using Azure.HabboHotel.GameClients;
using Azure.Messages;
using Azure.Messages.Parsers;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class RefreshCatalogue. This class cannot be inherited.
    /// </summary>
    internal sealed class RefreshCatalogue : Command
    {
        public RefreshCatalogue()
        {
            MinParams = 0;
            Description = TextManager.GetText("cmd_update_catalogue_desc");
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("cmd_global_refresh_permissions");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            {
                using (var adapter = AzureEmulator.GetDatabaseManager().GetQueryReactor())
                {
                    FurniDataParser.SetCache();
                    AzureEmulator.GetGame().GetItemManager().LoadItems(adapter);
                    AzureEmulator.GetGame().GetCatalog().Initialize(adapter);
                    FurniDataParser.Clear();
                }
                AzureEmulator.GetGame()
                    .GetClientManager()
                    .QueueBroadcaseMessage(
                        new ServerMessage(LibraryParser.OutgoingRequest("PublishShopMessageComposer")));
            }
        }
    }
}