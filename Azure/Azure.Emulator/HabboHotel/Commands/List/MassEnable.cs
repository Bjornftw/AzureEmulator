﻿#region

using System;
using System.Linq;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class MassEnable. This class cannot be inherited.
    /// </summary>
    internal sealed class MassEnable : Command
    {
        public MassEnable()
        {
            MinParams = 1;
            Description = "Mass room enable.";
            Usage = "[id]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("manager");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            ushort effectId;
            if (!ushort.TryParse(pms[0], out effectId)) return;

            var room = AzureEmulator.GetGame().GetRoomManager().GetRoom(session.GetHabbo().CurrentRoomId);
            room.GetRoomUserManager().GetRoomUserByHabbo(session.GetHabbo().Id);
            foreach (var user in room.GetRoomUserManager().GetRoomUsers().Where(user => !user.RidingHorse)) user.ApplyEffect(effectId);
        }
    }
}