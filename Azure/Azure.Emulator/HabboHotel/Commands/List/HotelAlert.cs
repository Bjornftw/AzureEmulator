﻿#region

using System;
using Azure.HabboHotel.GameClients;
using Azure.Messages;
using Azure.Messages.Parsers;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class HotelAlert. This class cannot be inherited.
    /// </summary>
    internal sealed class HotelAlert : Command
    {
        public HotelAlert()
        {
            MinParams = -1;
            Description = "Send hotel alert.";
            Usage = "[message]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("admin");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var str = string.Join(" ", pms);
            var message = new ServerMessage(LibraryParser.OutgoingRequest("BroadcastNotifMessageComposer"));
            message.AppendString(string.Format("{0}\r\n- {1}", str, session.GetHabbo().UserName));
            AzureEmulator.GetGame().GetClientManager().QueueBroadcaseMessage(message);
        }
    }
}