﻿#region

using System;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class MuteBots. This class cannot be inherited.
    /// </summary>
    internal sealed class Disco : Command
    {

        public Disco()
        {
            MinParams = 1;
            Description = "Creates a disco in the room.";
        }

        public override bool CanExecute(GameClient session)
        {
            var room = session.GetHabbo().CurrentRoom;
            return room == null || !room.CheckRights(session, true);
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var room = session.GetHabbo().CurrentRoom;
            room.DiscoMode = !room.DiscoMode;
        }
    }
}