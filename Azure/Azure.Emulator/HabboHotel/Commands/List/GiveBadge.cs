﻿#region

using System;
using Azure.Configuration;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class GiveBadge. This class cannot be inherited.
    /// </summary>
    internal sealed class GiveBadge : Command
    {
        public GiveBadge()
        {
            MinParams = 2;
            Description = "Give user badge.";
            Usage = "[username] [badge]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("manager");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var client = AzureEmulator.GetGame().GetClientManager().GetClientByUserName(pms[0]);
            if (client == null)
            {
                session.SendNotif(TextManager.GetText("user_not_found"));
                return;
            }
            client.GetHabbo().GetBadgeComponent().GiveBadge(pms[1], true, client, false);
            session.SendNotif(TextManager.GetText("command_badge_give_done"));
            AzureEmulator.GetGame()
                .GetModerationTool()
                .LogStaffEntry(session.GetHabbo().UserName, client.GetHabbo().UserName,
                    "Badge", string.Format("Badge given to user [{0}]", pms[1]));
        }
    }
}